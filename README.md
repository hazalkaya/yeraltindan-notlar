# YERALTINDAN NOTLAR

Gundelik hayatta lazim olan/kullanisli bilgiler.
-----------------------------------------------

- to change year on debian ( gnu/linux)
```
 # systemctl stop systemd-timesyncd.service 
 # date -s 'last year' 
```
- Postfix:
```
command to flush the mail queue:
# postfix flush

OR
# postfix -f

To see mail queue, enter:
# mailq

To remove all mail from the queue, enter:
# postsuper -d ALL

To remove all mails in the deferred queue, enter:
# postsuper -d ALL deferred
```


